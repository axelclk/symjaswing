## Symja Java Swing GUI - Symbolic Math System

Features:
* Java Swing GUI interface
* arbitrary precision integers, rational and complex numbers
* differentiation, integration, polynomials and linear algebra functions...
* function plots, parametric plots, 3D plots
* pretty printer output

See the Github pages of the library project
* [github.com/axkr/symja_android_library](https://github.com/axkr/symja_android_library)

Online demo: 
* [http://symjaweb.appspot.com/](http://symjaweb.appspot.com/)
	 
Run the `org.matheclipse.symja.Main` class to start the GUI